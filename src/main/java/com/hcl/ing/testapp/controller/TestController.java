package com.hcl.ing.testapp.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ing.testapp.model.TestData;

@RestController
@CrossOrigin("*")
public class TestController {
	private final static Logger logger = Logger.getLogger(TestController.class);

	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public TestData getRecodd() {
		TestData tdata = new TestData();
		tdata.setId("1");
		tdata.setName("kiran");
		logger.info("..getRecodd....");
		return tdata;
	}

	@RequestMapping(value = "/test-post", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String geSavetRecodd(TestData test) {

		test.setId("1");
		test.setName("kiran");
		logger.info("...getSaveRecodd" + test.getId());
		return "SUCCESS";
		// TestData tdata=new TestData();

	}

}
